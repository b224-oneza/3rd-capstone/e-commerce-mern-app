import { Button, Table } from "react-bootstrap";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

export default function AdminPanel({ productProp }) {

    // const { name, description, price, isActive } = productProp;
    const [products, setProducts] = useState([]);

    useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
        headers: {
          "Authorization": `Bearer ${localStorage.getItem("token")}`
        }
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setProducts(data)
        });
    }, []);

    const archive = (productId, isActive) => {
      fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify({
          isActive: isActive
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        if(data === true) {
          Swal.fire({
            title: "Archived Product!",
            icon: "info"
          })
        } else {
            Swal.fire({
            title: "There is an error!",
            icon: "info"
          })
        }
        })
    }

    const activate = (productId, isActive) => {
      fetch(`${process.env.REACT_APP_API_URL}/products/activate/${productId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          isActive: isActive,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);

          if (data === true) {
            Swal.fire({
              title: "Product Activated!",
              icon: "info",
            });
          } else {
            Swal.fire({
              title: "There is an error!",
              icon: "info",
            });
          }
        });
    };

    return (
      <>
        <div className="mb-3">
          <h1 className="text-center">Admin Panel</h1>
          <div className="text-center">
            <Button variant="primary" className="m-1">
              Add New Product
            </Button>
            <Button variant="warning" className="m-1">
              Show User Orders
            </Button>
          </div>
        </div>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Availability</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          {products && products?.map((product) => {
            return (
              <tr>
                <th>{product.name}</th>
                <th>{product.description}</th>
                <th>{product.price}</th>
                <th>{product.isActive ? "Active" : "Inactive"}</th>
                <th>
                  <Button variant="primary">Update</Button> <br></br>
                  {!product.isActive  ? (
                  
                    <Button variant="success" onClick={() => activate(product._id, product.isActive)}>Activate</Button>
                  
                ) : (
                  
                    <Button variant="danger" onClick={() => archive(product._id, product.isActive)}>Disable</Button>
                  
                )}
                </th>
              </tr>
            );
          })}

          </tbody>
        </Table>
      </>
    );
}