import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>One of our recent best sellers</Card.Title>
            <Card.Text>
              Double Ham & Cheese - Together with freshly baked brioche, in the
              inside is a creamy egg together with double sliced ham, covered
              with slices of cheese and topped with the Sando's special sauces
              and flakes.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>Good food supermacy</Card.Title>
            <Card.Text>
              Nothing brings people together like good food.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>Sometimes all you need is a hug...</Card.Title>
            <Card.Text>
              and a bunch if brioche slice, to make it through any day.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
