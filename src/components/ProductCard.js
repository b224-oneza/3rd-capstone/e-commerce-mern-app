import { useState, useEffect } from "react";
import { Row, Col, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard({ productProp }) {

  /*
		Use the state hook for this component to be able to store its state.
		States are used to keep track of the information related to individual components

		Syntax: 
			const [getter, setter] = useState(initialGetterValue)
	*/

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);
  // console.log(useState(0));

  function checkout() {
    if (count < 31) {
      setCount(count + 1);
      // console.log('Enrollee: ' + count)
      setSeats(seats - 1);
      // console.log('Seats: ' + seats)
    } // else {
    // alert("No more seats available.")
    // }
  }

  useEffect(() => {
    if (seats === 0) {
      alert("No more seats available.");
    }
  }, [seats]);

  const { name, description, price, _id } = productProp;

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={6}>
        <Card className="cardHighlight2 p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PHP {price}.00</Card.Text>
            <Button className="bg-primary" as={Link} to={`/product/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
    
  );
}
