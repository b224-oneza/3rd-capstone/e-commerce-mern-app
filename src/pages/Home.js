import Banner from "../components/Banner";
// import CourseCard from '../components/CourseCard';
import Highlights from "../components/Highlights";

export default function Home() {
  const data = {
    title: "Sando.ph",
    content: "Sando - House of Sandwiches offers the best homemade sandwiches inspired by Asian and American cuisi",
    destination: "/courses",
    label: "Shop now!",
  };

  return (
    <>
      <Banner data={data} />
      <Highlights />
      {/*<CourseCard/>*/}
    </>
  );
}
